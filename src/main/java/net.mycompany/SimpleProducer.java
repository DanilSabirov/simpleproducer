package net.mycompany;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;
import java.util.UUID;

public class SimpleProducer {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Enter topic name");
            return;
        }

        String topicName = args[0];

        Properties properties = new Properties();
        properties.put("bootstrap.servers", "localhost:9092");
        properties.put("acks", "all");
        properties.put("retries", "0");
        properties.put("key.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");

        properties.put("value.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");

        Producer<String, String> producer = new KafkaProducer<>(properties);

        while (true) {
            String value = UUID.randomUUID().toString();
            producer.send(new ProducerRecord<>(topicName, value));
            System.out.println("Message sent: " + value);
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {

            }
        }
    }
}
